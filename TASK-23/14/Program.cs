﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, string>();

            dict.Add("Monday", "Weekday");
            dict.Add("Tuesday", "Weekday");
            dict.Add("Wednesday", "Weekday");
            dict.Add("Thursday", "Weekday");
            dict.Add("Friday", "Weekday");
            dict.Add("Saturday", "Weekend");
            dict.Add("Sunday", "Weekdend");
            
            foreach (var x in dict)
            {

                Console.WriteLine($"{x.Key} is a {x.Value}");


            }
        }
    }
}
