﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11
{
    class Program
    {
        static void Main(string[] args)
        {


            string[,] birthdayList = new string[3, 3] { { "Earl", "4", "October" }, { "Jodee", "31", "November" }, { "Jeff", "17", "May" } };


            Console.WriteLine($"Name, Birthday, Month");
            for (int i = 0; i < birthdayList.GetLength(0); i++)
            {
                for (int x = 0; x < birthdayList.GetLength(0); x++)
                {
                    if (x != birthdayList.GetLength(0))
                    {
                        Console.Write(birthdayList[i, x] + ",");
                    }
                    else
                    {
                        Console.Write(birthdayList[i, x]);
                    }
                }
               
            }
        }
    }
}


