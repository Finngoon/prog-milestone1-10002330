﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] array = new int[7] { 34, 45, 21, 44, 67, 88, 86 };

            int[] even = (from y in array where ((y % 2) == 0) select y).ToArray();

            List<int> evennumbers = even.ToList();

            Console.WriteLine("The even numbers are: ");
            Console.WriteLine(string.Join(",", evennumbers));

        }
    }
}
