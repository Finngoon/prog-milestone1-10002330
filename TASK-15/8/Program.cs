﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8
{
    class Program
    {
        static void Main(string[] args)
        {

            int one;
            int two;
            int three;
            int four;
            int five;
            int total = 0;


            Console.WriteLine("Please enter five numbers you wish to add together (Press enter after each number)");
            one = int.Parse(Console.ReadLine());
            two = int.Parse(Console.ReadLine());
            three = int.Parse(Console.ReadLine());
            four = int.Parse(Console.ReadLine());
            five = int.Parse(Console.ReadLine());

            List<int> list = new List<int>(new int[] { one, two, three, four, five });

            int[] array = new int [5] { one, two, three, four, five };
            Console.Clear();



            Console.WriteLine("Your input was:");
            Console.WriteLine("Taken from the Array:");
            Console.WriteLine(String.Join(",", array));

            Console.WriteLine("Taken from the List:");
            Console.WriteLine(string.Join(",", list));

            total = one + two + three + four + five;

            Console.WriteLine($"Your answer is: {total}");
            




        }
    }
}
