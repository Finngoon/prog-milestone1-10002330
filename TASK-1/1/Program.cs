﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {

            int age = 0;

            var name = "";

            Console.WriteLine("Hello, Please enter your name: ");
            name = Console.ReadLine();

            Console.WriteLine("Please enter your age: ");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hello {name}. You are {age} years old.");

            Console.WriteLine("Hello {0}. You are {1} years old.", name, age);

            Console.WriteLine("Hello " + name + ". You are " + age + " years old");

           
            
        }
    }
}
