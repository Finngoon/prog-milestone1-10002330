﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    class Program
    {
        static void Main(string[] args)
        {

            var dict = new Dictionary<string, string>();

            dict.Add("Water", "Blue");
            dict.Add("Fire", "Red");
            dict.Add("Grass", "Green");

            if (dict.ContainsKey("Water"))
            {
                Console.WriteLine($"Waters colour is {dict["Water"]}");
            }


        }
    }
}
