﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7
{
    class Program
    {
        static void Main(string[] args)
        {
            int input = 0;
            int answer = 0;

            Console.WriteLine("Please enter how many TB your computer is");
            input = int.Parse(Console.ReadLine());

            answer = input * 1024;

            Console.WriteLine($"Your computer is {answer} GB");


        }
    }
}
