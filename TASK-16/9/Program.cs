﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9
{
    class Program
    {
        static void Main(string[] args)
        {

            string word;
            int amount; 

            Console.WriteLine("Please enter a word:");
            word = Console.ReadLine();
            amount = word.Length;

            Console.WriteLine($"Your word was {word} and it has {amount} characters.");

        }
    }
}
